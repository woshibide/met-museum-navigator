setUserPreferredTheme();
let isLoading = false;
let debounceTimeout;

function loadMoreImages() {
    if (!isLoading) {
        isLoading = true;

        document.getElementById('loading-spinner').classList.remove('d-none');
        document.getElementById('spinner-text').classList.remove('d-none');

        const imgContainer = document.querySelector('.image-container');
        fetch(`/load_more_images?offset=${imgContainer.childElementCount}`)
            .then(response => response.json())
            .then(data => {
                // Hide the loading spinner
                document.getElementById('loading-spinner').classList.add('d-none');
                data.random_images.forEach(imageData => {
                    const col = Math.floor(Math.random() * 3) + 1;
                    const start_col = Math.floor(Math.random() * (5 - col)) + 1;
                    const img = document.createElement('img');
                    img.src = imageData.primaryImage;
                    img.alt = imageData.title;
                    img.title = imageData.title;
                    img.classList.add('image'); // lazy load could be used
                    // clickable image
                    const link = document.createElement('a');
                    link.href = imageData.primaryImage;
                    link.target = "_blank";
                    link.appendChild(img);
                    const imageItem = document.createElement('div');
                    imageItem.classList.add('image-item');
                    imageItem.style.gridColumnStart = start_col;
                    imageItem.style.gridColumnEnd = `span ${col}`;
                    imageItem.appendChild(link);
                    // title 
                    const title = document.createElement('p');
                    title.textContent = imageData.title;
                    imageItem.appendChild(title);
                    imgContainer.appendChild(imageItem);
                });
                // Reset the flag when the API call is complete and images are added
                isLoading = false;
                
                document.getElementById('loading-spinner').classList.add('d-none');
                document.getElementById('spinner-text').classList.add('d-none');

            })
            .catch(error => {
                console.error('Error:', error);
                isLoading = false;
                document.getElementById('loading-spinner').classList.add('d-none');
            });
    }
}
window.addEventListener('scroll', () => {
    if (debounceTimeout) {
        clearTimeout(debounceTimeout);
    }
    debounceTimeout = setTimeout(() => {
        if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
            loadMoreImages();
        }
    }, 200);
});


function setUserPreferredTheme() {
    const userPrefersDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
    const html = document.querySelector('html');
    if (userPrefersDark) {
        html.setAttribute('data-theme', 'dark');
    } else {
        html.setAttribute('data-theme', 'light');
    }
    changeSvgFillColor();
}

function changeSvgFillColor() {
    // is there a dictionary type of abstraction for that?
    const arrowUp = document.getElementById('go-to-top');
    const themeIcon = document.getElementById('theme-icon');
    const minimizeSearch = document.getElementById('minimize-search');

    arrowUp.style.filter = `invert(${document.documentElement.getAttribute('data-theme') === 'light' ? 0 : 100}%)`;
    themeIcon.style.filter = `invert(${document.documentElement.getAttribute('data-theme') === 'light' ? 0 : 100}%)`;
    minimizeSearch.style.filter = `invert(${document.documentElement.getAttribute('data-theme') === 'light' ? 0 : 100}%)`;

}

document.getElementById('theme-toggle').addEventListener('click', function () {
    console.log('button pressed');
    var html = document.querySelector('html');
    var currentTheme = html.getAttribute('data-theme');
    console.log('currentTheme:', currentTheme);
    if (currentTheme === 'light') {
        html.setAttribute('data-theme', 'dark');
    } else {
        html.setAttribute('data-theme', 'light');
    }
    changeSvgFillColor();
});


document.getElementById('go-to-top').addEventListener('click', function() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
});

window.addEventListener('scroll', function() {
    const goToTopBtn = document.getElementById('go-to-top');
    if (window.scrollY > window.innerHeight) {
        goToTopBtn.classList.remove('d-none');
    } else {
        goToTopBtn.classList.add('d-none');
    }
});


