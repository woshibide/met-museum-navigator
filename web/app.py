from flask import Flask, render_template, request, redirect, url_for, jsonify
import requests, random, time


app = Flask(__name__)
print("[%s] --- Server starting..." % time.strftime("%H:%M:%S"))

TITLE_WORDS = ["andiron", "armchair", "pistol", "gusoku", "armor"]



def search_objects(search_query):
    print("[%s] --- Searching objects for query: %s" % (time.strftime("%H:%M:%S"), search_query))
    url = f"https://collectionapi.metmuseum.org/public/collection/v1/search?q={search_query}"
    response = requests.get(url)
    search_results = response.json()
    object_ids = search_results.get("objectIDs", [])
    return object_ids


def get_object_data(object_id):
    print("[%s] --- Getting object data for object ID: %s" % (time.strftime("%H:%M:%S"), object_id))
    url = f"https://collectionapi.metmuseum.org/public/collection/v1/objects/{object_id}"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        if data["primaryImage"]:
            return {
                "objectId": data["objectID"],
                "title": data["title"],
                "primaryImage": data["primaryImage"],
                "url": data["objectURL"],
            }
    else:
        print(f"Error fetching object data for object ID {object_id}: {response.status_code} - {response.reason}")
        
    return None


def get_object_ids(search_query, limit=10):
    print("[%s] --- get object ids" % time.strftime("%H:%M:%S"))
    object_ids = search_objects(search_query)
    return object_ids[:limit]


def get_random_images(title_words, offset=0, limit=10):
    print("[%s] --- Getting random images" % time.strftime("%H:%M:%S"))
    random_images = []
    attempts = 0
    # Adding an attempts counter to prevent infinite loops
    while len(random_images) < limit and attempts < 5:  
        attempts += 1
        random_title = random.choice(title_words)
        print("[%s] --- Random title selected: %s" % (time.strftime("%H:%M:%S"), random_title))
        object_ids = search_objects(random_title)

        if not object_ids:
            continue

        sliced_object_ids = random.sample(object_ids[offset:offset + limit], min(limit, len(object_ids[offset:offset + limit])))

        for object_id in sliced_object_ids:
            object_data = get_object_data(object_id)

            if object_data and object_data["primaryImage"]:
                print("[%s] --- --- Adding object data for object ID %s to random images" % (time.strftime("%H:%M:%S"), object_id))
                random_images.append(object_data)

            if len(random_images) >= limit: 
                break

    return random_images


def generate_items_with_col(image_data):
    items_with_col = []
    for img_data in image_data:
        col = random.randint(1, 3)
        if col == 3:
            start_col = random.randint(1, 2)
        else:
            start_col = random.randint(1, 4 - col + 1)
        items_with_col.append({'url': img_data["primaryImage"], 'col': col, 'start_col': start_col, 'title': img_data["title"]})
    return items_with_col



@app.route("/")
def index():
    print("[%s] --- Index route accessed" % time.strftime("%H:%M:%S"))
    return redirect(url_for('search_form'))


@app.route('/load_more_images')
def load_more_images():
    print("[%s] --- Load more images route accessed" % time.strftime("%H:%M:%S"))
    offset = int(request.args.get('offset', 0))
    random_images = get_random_images(TITLE_WORDS, offset=offset, limit=10)
    return jsonify(random_images=random_images)


@app.route("/search_form")
def search_form():
    print("[%s] --- Search form route accessed" % time.strftime("%H:%M:%S"))
    
    random_images = get_random_images(TITLE_WORDS)
    items_with_col = generate_items_with_col(random_images)

    return render_template("base.html", items_with_col=items_with_col, title_words=TITLE_WORDS)



@app.route("/search")
def search():
    search_query = request.args.get('search_query')
    print("[%s] --- Search route accessed with query: %s" % (time.strftime("%H:%M:%S"), search_query))
    
    count = 25

    object_ids = search_objects(search_query)
    if not object_ids:
        return render_template("not_found.html", search_query=search_query)


    object_data_list = [get_object_data(object_id) for object_id in object_ids[:count]]
    
    image_data = []
    for object_data in object_data_list:
        if object_data["primaryImage"]:
            url = object_data["primaryImage"]
            title = object_data["title"]
            print("[%s] --- --- Processing object data with title: %s" % (time.strftime("%H:%M:%S"), title))
            image_data.append({"url": url, "title": title})  

    items_with_col = generate_items_with_col(image_data)
    return render_template("home.html", items_with_col=items_with_col)




if __name__ == "__main__":
    app.run(debug=True)
