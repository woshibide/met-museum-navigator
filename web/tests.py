import unittest
import app
import base64
from io import BytesIO
from PIL import Image

class TestMetMuseumAPI(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_index(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertIn('/search_form', response.headers['Location'])

    def test_search_form(self):
        response = self.app.get('/search_form')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Search the Met Museum Collection', response.data)
        
    def test_search(self):
        response = self.app.get('/search?search_query=chair&count=5')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<img src="', response.data)

    def test_search_objects(self):
        result = app.search_objects("chair")
        self.assertIsInstance(result, list)
        self.assertGreater(len(result), 0)

    def test_get_object_data(self):
        object_data = app.get_object_data(1)
        self.assertIsInstance(object_data, dict)
        self.assertIn("objectID", object_data)
        self.assertIn("primaryImage", object_data)

    def test_create_thumbnail(self):
        url = "https://images.metmuseum.org/CRDImages/ad/original/DP251139.jpg"
        try:
            thumbnail = app.create_thumbnail(url)
            img = Image.open(BytesIO(thumbnail))
            self.assertIsInstance(thumbnail, bytes)
            self.assertEqual(img.size, (300, 300))
        except IOError:
            self.skipTest("External image not available, skipping test.")

if __name__ == '__main__':
    unittest.main()
