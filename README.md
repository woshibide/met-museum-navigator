This project is a simple web application that utilizes the [Met Museum Collection API](https://metmuseum.github.io/) to search for artwork images based on a user-provided title. The application displays the images in a responsive grid format, allowing users to explore the Met Museum's vast collection of artworks.

![alt text](preview.png "project preview")

# 🎨 MET Museum Gallery API

🏛️ Welcome to the MET Gallery! This website was born from a designer's frustration while searching for high-resolution images of interesting objects set against simple backgrounds. The internet seemed to lack such a resource, and here it is for you to explore. Delve into the [Metropolitan Museum of Art](https://www.metmuseum.org/) collection, which includes contemporary masterpieces, ageless classics, and a wide array of artistic treasures.

This is not an official website of MET.

## 🚀 Usage

1. Clone the repository and navigate to the project directory.
2. Set up a Python virtual environment by running `python -m venv nameMe` or `python3 -m venv venv`. If you are not familiar with Python virtual environments, refer to the [official documentation](https://docs.python.org/3/tutorial/venv.html) for more information.
3. Activate the virtual environment by running `source nameMe/bin/activate` (for Linux and macOS) or `venv\Scripts\activate` (for Windows).
4. Install the required packages from `requirements.txt` by running `pip install -r requirements.txt`.
5. Run the application using `flask run`. Command `flask run --debug` will set you free from restarting server each time you make updates.
6. Open your web browser and go to `http://127.0.0.1:5000/` to access the application.

## 📚 Documentation

For more information about the Met Museum Collection API and its usage, visit the [official documentation](https://metmuseum.github.io/).

## 🤝 Contributing

Feel free to contribute to this project by submitting pull requests or opening issues. Your feedback and suggestions are always welcome.

## 🚧 Planned Features

- Rotate long images
- Subtle animation
- "Copy URL" button 
- Display objectName, objectEndDate, artistDisplayName, and medium on image hover
- "Additional Images" button 
- Fix occasional image loading issues

### Additional Ideas

- List view
- More API's to use
- Report button for images against not-plain black background

### Error Handling

- MET museum has 96 million pixel image...
